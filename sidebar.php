
<?php if ( is_page( 'about-us' )): ?>
	<div class="col-xs-12 col-sm-3">
		<div style="margin: 10px 0 60px;"><img src="/wp-content/uploads/2015/07/widget_frog_bg.png" alt=""></div>
	</div>
	<div class="col-xs-12 col-sm-3">
<?php elseif ( is_page( 'pricing' )): ?>
	<div class="col-xs-12 col-sm-3">
		<div style="margin: 10px 0 60px;"><img src="/wp-content/uploads/2015/07/widget_frog_bg.png" alt=""></div>
	</div>
	<div class="col-xs-12 col-sm-3 sidebar">
<?php else: ?>
	<div class="col-xs-12 col-sm-3 sidebar">
<?php endif ?>

	<?php if ( ! dynamic_sidebar( 'page' ) ): ?>

	<h3>Sidebar Setup</h3>
	<p>Please add widgets to the page sidebar to have them display here.</p>

	<?php endif; ?>

</div>