<?php
/*
  Template Name: FAQ Template
*/

?>

<?php get_header(); ?>

<style>
.views-field-title h4 {
	font-size: 1.2em;
}
.views-field-view-node {text-align: right;}
a.anchor {display: block; position: relative; top: -70px; visibility: hidden;}
a.top-anchor {display: block; position: relative; top: -100px; visibility: hidden;}
</style>

  <div class="container-fluid container-inside-text">
    <div class="row">

      <div class="col-md-9">

	<div class="page-header">
		<h1><?php the_title(); ?></h1>
	</div>

      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<?php if ( have_rows('faq_sections') ): ?>
			<a name="context-info" id="context-info" class="top-anchor" href="#"></a>
			<div class="list faq faq-questions">
			<?php $q_cnt = 0; ?>
			<?php while (  have_rows('faq_sections') ): the_row(); ?>

			<?php if(get_sub_field('section_title')) { ?>
				<h3><?php the_sub_field('section_title'); ?></h3>
				<?php $q_cnt = $q_cnt + 1 ?>
			<?php	} ?>

				<?php if ( have_rows('section_item') ): ?>
					<ol>
					<?php $i = 1; ?>
					<?php while (  have_rows('section_item') ): the_row(); ?>

						<li><a href="#q-<?php echo $q_cnt; ?>-<?php echo $i++; ?>"><?php the_sub_field('question'); ?></a></li>

					<?php endwhile; ?>
					</ol>

				<?php endif; ?>

			<?php endwhile; ?>
			</div>
		<?php endif; ?>

		<?php if ( have_rows('faq_sections') ):
			$q_cnt = 0;
			while (  have_rows('faq_sections') ): the_row(); ?>

				<?php if(get_sub_field('section_title')) { ?>
					<h3><?php the_sub_field('section_title'); ?></h3>
					<?php $q_cnt = $q_cnt + 1 ?>
				<?php	} ?>

				<?php if ( have_rows('section_item') ): ?>
					<?php $i = 1; ?>
					<?php while (  have_rows('section_item') ): the_row(); ?>

						<div class="row">

							<div class="views-field-title">
								<span class="field-content"><a name="q-<?php echo $q_cnt; ?>-<?php echo $i; ?>" id="q-<?php echo $q_cnt; ?>-<?php echo $i++; ?>" class="anchor" href="#"></a><br /><h4><?php the_sub_field('question'); ?></h4></span>
							</div>
							<div class="views-field-field-faq-answer-value">
								<div class="field-content"><?php the_sub_field('answer'); ?></div>
							</div>
							<div class="views-field-view-node">
								<span class="field-content"><a href="#context-info">FAQ list ^</a></span>
							</div>

						</div>

					<?php endwhile; ?>

				<?php endif; ?>

			<?php endwhile;

		endif; ?>

        <?php endwhile; else: ?>

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>

        <?php endif; ?>


      </div>

       <?php get_sidebar( 'faq' ); ?>

    </div>

<?php get_footer(); ?>