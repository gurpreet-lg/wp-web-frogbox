<?php get_header(); ?>
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
		<div class="title-section">
		<div class="title-home-section">
			<h1>Moving boxes and supplies delivered to you.</h1>
			<h3>When you are done with your move, we pick up your empty boxes, so you can relax.</h3>
			<div class="btn-home-container"><a href="/order" class="btn btn-home">Get started</a></div>
		</div>
      <div class="banner-chevron"><i class="fa fa-chevron-down" aria-hidden="true"></i></div>
		</div>

      <?php //if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php //the_content(); ?>
      <?php// endwhile; endif; ?>

      <!-- div class="row">
      	<img src="/wp-content/uploads/2015/05/home-banner.jpg" alt="">
      </div -->

      </div>
    </div>
<style type="text/css">
.the-steps-link {
	display: block;
}
.the-steps-link p {
	color: #333;
}
</style>
    <div class="container container-inside">
		<div class="row">
			<div class="col-md-4">
				<a href="/pricing/" class="the-steps-link">
				<img src="/wp-content/uploads/2015/07/inner-step1.jpg" alt="Step 1" style="display:block;margin:0 auto;">
				<!-- div style="max-height: 460px;border: 1px solid #e7e7e8;border-radius:6px;margin-bottom:12px;padding:20px;text-align:center;">
					<div style=""><img src="/wp-content/uploads/2015/07/home-step1.jpg" alt="" style="vertical-align:top;"></div>
					<h2 style="text-align:left;">1. Schedule Frogbox</h2>
					<p style="text-align:left;">Schedule your delivery and pick up of your moving and packing supplies.</p>
				</div -->
				</a>
			</div>
			<div class="col-md-4">
				<a href="/pricing/" class="the-steps-link">
				<img src="/wp-content/uploads/2015/07/inner-step2.jpg" alt="Step 2" style="display:block;margin:0 auto;">
				<!-- div style="max-height: 460px;border: 1px solid #e7e7e8;border-radius:6px;margin-bottom:12px;padding:32px;text-align:center;">
					<div style=""><img src="/wp-content/uploads/2015/07/frogbox-open-live.jpg" alt="" style="vertical-align:top;"></div>
					<h2 style="text-align:left;">2. Pack and Move</h2>
					<p style="text-align:left;">You pack and move to your new pad. Faster, easier and more secure with Frogboxes.</p>
				</div -->
				</a>
			</div>
			<div class="col-md-4">
				<a href="/pricing/" class="the-steps-link">
				<img src="/wp-content/uploads/2015/07/inner-step3.jpg" alt="Step 3" style="display:block;margin:0 auto;">
				<!-- div style="max-height: 460px;border: 1px solid #e7e7e8;border-radius:6px;margin-bottom:12px;padding:20px;text-align:center;">
					<div style=""><img src="/wp-content/uploads/2015/07/home-step3.jpg" alt="" style="vertical-align:top;"></div>
					<h2 style="text-align:left;">3. Unpack and Relax</h2>
					<p style="text-align:left;">When you finish unpacking, we come pick up the empty Frogboxes from your new pad.</p>
				</div -->
				</a>
			</div>
		</div>
	</div>

<div class="container container-inside" style="margin: 20px auto 0;">
	<div class="row">
		<div class="col-sm-12"><p style="padding-left: 10px;padding-right: 10px;color:#939597;">Moving your home or office? Don't waste time with cardboard moving boxes, at Frogbox we offer a different approach. We rent reusable plastic moving boxes and we deliver to your home. Packing and moving will be much more convenient with our boxes and supplies. Moving boxes, wardrobes, dollies, packing paper, bubble wrap, mattress covers and more! When you are finished moving we pick up the boxes and reusable moving supplies so there is less waste than a traditional move.</p></div>
	</div>
</div>

<style type="text/css">
.responsive {
	/*width:100px;*/
	margin: 30px auto;
}
.responsive div.custom-sf-slide {
	margin: 0 8px;
	padding: 4px;
	border: 1px solid #8CC640;
	border-radius: 6px;
}
.slick-prev, .slick-next {
	width: 28px;
	height: 26px;
	margin-top: -20px;
}
.slick-prev:before, .slick-next:before{
  font-size: 30px;
  color:#f77f2f;
}
</style>
<div class="container container-inside" style="margin: 40px auto 0;">
	<div class="row">
		<div class="col-sm-12">
			<h2 style="text-align: center;">Moving Supplies - Frogbox is your one-stop shop</h2>

			<div class="responsive">
			  <div class="custom-sf-slide"><a href="/pricing/"><img src="/wp-content/uploads/2017/03/slides-1.jpg"></a></div>
			  <div class="custom-sf-slide"><a href="/pricing/"><img src="/wp-content/uploads/2017/03/slides-2.jpg"></a></div>
			  <div class="custom-sf-slide"><a href="/pricing/"><img src="/wp-content/uploads/2017/03/slides-3.jpg"></a></div>
			  <div class="custom-sf-slide"><a href="/pricing/"><img src="/wp-content/uploads/2017/03/slides-4.jpg"></a></div>
			  <div class="custom-sf-slide"><a href="/pricing/"><img src="/wp-content/uploads/2017/03/slides-6.jpg"></a></div>
			  <div class="custom-sf-slide"><a href="/pricing/"><img src="/wp-content/uploads/2017/03/slides-7.jpg"></a></div>
			  <div class="custom-sf-slide"><a href="/pricing/"><img src="/wp-content/uploads/2017/03/slides-8.jpg"></a></div>
			  <div class="custom-sf-slide"><a href="/pricing/"><img src="/wp-content/uploads/2017/03/slides-9.jpg"></a></div>
			  <div class="custom-sf-slide"><a href="/pricing/"><img src="/wp-content/uploads/2017/03/slides-10.jpg"></a></div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function($) {
	$('.responsive').slick({
	  dots: false,
	  arrows: true,
	  infinite: true,
	  autoplay: true,
    autoplaySpeed: 3000,
    pauseOnFocus: true,
    pauseOnHover: true,
	  speed: 300,
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
});
</script>

	<div class="container container-inside">
	<script type="text/javascript">
	// count up counter implementation
	var START_DATE = new Date("March 8, 2012 13:30:00"); // put in the starting date here
	var INTERVAL = 1; // in seconds
	var INCREMENT = 0.00532; // increase per tick
	var START_VALUE = 968399; // initial value when it's the start date
	var count = 0;

	window.onload = function()
	{
	       var msInterval = INTERVAL * 1000;
	       var now = new Date();
	       count = parseInt((now - START_DATE)/msInterval) * INCREMENT + START_VALUE;
	       document.getElementById('counter').innerHTML = formatNumber(count.toFixed(3));
	       setInterval("count += INCREMENT; document.getElementById('counter').innerHTML = formatNumber(count.toFixed(3));", msInterval);
	}

	function formatNumber(n){
	       var s=(''+n).split('.');
	       s[0]=s[0].split('').reverse().join('').match(/\d{1,3}/gi).join(',').split('').reverse().join('');
	       return(s.join('.'));
	}
	</script>
		<div class="row">
			<div class="col-sm-12" style="text-align:center;margin:20px 0 0;">
				<a href="/about-us/#million"><h3 style="margin: 20px auto 0;border: 1px solid;padding: 20px 0;border-radius: 6px;width: 100%;"><span id="counter" style="color: #939597;"></span> <nobr>Frog<span style="color:#00A5C7;">boxes</span> Re-Used!</nobr></h3></a>
			</div>
		</div>
	</div>

	<div class="container container-inside" style="margin-bottom:20px;">
		<div class="row">
			<h2 style="margin:50px 0 0;text-align: center;font-size:3em;">Free Local Delivery And Pick Up</h2>
		</div>
		<div class="row">
			<h2 style="margin:20px 0 0;text-align: center;">Choose one of our locations:</h2>
			<div style="margin:5px auto 2em;width:30%;">
			<select name="" id="" class="form-control"  name="sort" onchange="javascript:location.href = this.options[this.selectedIndex].value">
			<option value="#">Select one</option>
			<option value="/abbotsford-langley">Abbotsford-Langley</option>
			<option value="/boise">Boise</option>
			<option value="/calgary">Calgary</option>
			<option value="/durham">Durham Region</option>
			<option value="/edmonton">Edmonton</option>
			<option value="/hamilton-oakville">Hamilton-Oakville</option>
			<option value="/kelowna">Kelowna - Penticton</option>
			<option value="/kitchener-waterloo">Kitchener-Waterloo</option>
			<option value="/lethbridge">Lethbridge</option>
			<option value="/london">London</option>
			<option value="/mississauga">Mississauga</option>
			<option value="/ottawa">Ottawa</option>
			<!-- option value="/saskatoon">Saskatoon</option -->
			<option value="/seattle">Seattle</option>
			<!-- option value="/st-johns">St. John's</option -->
			<option value="/surrey-delta">Surrey-Delta</option>
			<option value="/toronto">Toronto</option>
			<option value="/vancouver">Vancouver</option>
			<option value="/victoria">Victoria</option>
			<option value="/winnipeg">Winnipeg</option>
		</select>
			</div>
		</div>
		<div class="row">
			<div class="mobile-map"><img src="/wp-content/uploads/2018/03/map-v7.png" alt=""></div>
			<div class="location-map">
			    <img src="/wp-content/uploads/2018/03/map-v7.png">
			    <a href="/Boise" style="top:59.5%;left:13.4%"><span>Boise</span></a>
			    <a href="/Calgary" style="top: 30.5%;left: 15.6%;"><span>Calgary</span></a>
			    <a href="/durham" style="top: 51.3%;left: 60.9%;"><span>Durham Region</span></a>
			    <a href="/Edmonton" style="top: 21%;left: 18%;"><span>Edmonton</span></a>
			    <a href="/Fraser-Valley" style="top: 38%;left: 5.8%;"><span>Fraser Valley</span></a>
			    <!-- a href="/Halifax" style="top: 56.7%;left: 82.5%;"><span>Halifax</span></a -->
			    <a href="/hamilton-oakville" style="top: 56.5%;left: 56.8%;"><span>Hamilton-Oakville</span></a>
			    <a href="/kitchener-waterloo" style="top: 55.8%;left: 55%;"><span>Kitchener-Waterloo</span></a>
			    <a href="/Lethbridge" style="top: 38.7%;left: 18.7%;"><span>Lethbridge</span></a>
			    <a href="/London" style="top: 59.6%;left: 53.1%;"><span>London</span></a>
			    <a href="/Mississauga" style="top: 53.2%;left: 58.1%;"><span>Mississauga</span></a>
			    <a href="/Okanagan" style="top: 34.1%;left: 10.3%;"><span>Okanagan</span></a>
			    <a href="/Ottawa" style="top: 41.2%;left: 67.6%;"><span>Ottawa</span></a>
			    <!-- a href="/Saskatoon" style="top: 29.1%;left: 24.5%;"><span>Saskatoon</span></a -->
			    <a href="/Seattle" style="top: 45%;left: 5.8%;"><span>Seattle</span></a>
			    <!-- a href="/St-Johns" style="top: 45.3%;left: 96.2%;"><span>St. John's</span></a -->
			    <a href="/Toronto" style="top: 52.2%;left: 59.4%;"><span>Toronto</span></a>
			    <!-- a href="/minneapolis-st-paul" style="top: 55.3%;left: 43%;"><span>Twin Cities</span></a -->
			    <a href="/Vancouver" style="top: 37%;left: 3.4%;"><span>Vancouver</span></a>
			    <a href="/Victoria" style="top: 42.4%;left: 3.7%;"><span>Victoria</span></a>
			    <a href="/Winnipeg" style="top: 35.6%;left: 38.1%;"><span>Winnipeg</span></a>
			</div>
		</div>

	</div>

<div class="container container-inside" style="margin-bottom:20px;">
<div class="row">
<div class="col-md-8 col-md-offset-2" style="text-align: center;">
<h3>What Fits in a Frog<span style="color: #00a5c7;">box</span>?</h3>
<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/USglAkr55xs?VQ=HD720&amp;rel=0"></iframe>
</div>
</div>
</div>
</div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">

          <?php if ( dynamic_sidebar( 'front-left' ) ); ?>

        </div>
        <div class="col-md-4">

          <?php if ( dynamic_sidebar( 'front-center' ) ); ?>

       </div>
        <div class="col-md-4">

          <?php if ( dynamic_sidebar( 'front-right' ) ); ?>

        </div>
      </div>

<?php get_footer(); ?>