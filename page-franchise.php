<?php
/*
  Template Name: Franchise Template
*/

?>

<?php get_header(); ?>

  <div class="container-fluid container-inside-text">
	<div class="row">
		<div class="col-sm-12 page-header"><h1><?php the_title(); ?></h1></div>
	</div>
    <div class="row">

      <div class="col-xs-12 col-sm-9">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <!-- <div class="page-header">
            <h1><?php the_title(); ?></h1>
          </div> -->

          <?php //the_content(); ?>


<?php

/*
*  Loop through a Flexible Content field and display it's content with different views for different layouts
*/

while(the_flexible_field("content")): ?>

	<?php if(get_row_layout() == "intro_section"): // layout: Content
		$image = get_sub_field('image');
	?>
	<div class="row">
		<div class="col-sm-7">
			<img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>" />
		</div>

		<div class="col-sm-5">
			<?php the_sub_field("serving_locations"); ?>
		</div>
	</div>
	<hr>
	<?php elseif(get_row_layout() == "promo_area"): // layout: Content ?>
	<div class="row">
		<?php the_sub_field("full_width_content"); ?>
	</div>
	<hr>
      <?php elseif(get_row_layout() == "quotes"): // layout: Content ?>
	<div class="row">
		<h3>Reviews:</h3>
		<?php if(get_sub_field("quote1")): ?>

			<div class="col-sm-4">
				<div class="more"><?php the_sub_field("quote1"); ?>	
				<h4 style="text-align:right;">- <?php the_sub_field("quote1_name"); ?></h4>
				</div>
			</div>

		<?php endif; ?>

		<?php if(get_sub_field("quote2")): ?>

			<div class="col-sm-4">
				<div><?php the_sub_field("quote2"); ?></div>
				<h4 style="text-align:right;">- <?php the_sub_field("quote2_name"); ?></h4>
			</div>

		<?php endif; ?>

		<?php if(get_sub_field("quote3")): ?>

			<div class="col-sm-4">
				<div><?php the_sub_field("quote3"); ?></div>
				<h4 style="text-align:right;">- <?php the_sub_field("quote3_name"); ?></h4>
			</div>

		<?php endif; ?>
	</div>
	<hr>

	<?php
		elseif(get_row_layout() == "map_area"): // layout: Content 
		$map_included = "Y";
	?>
		<div class="row">
			<div class="col-sm-12">
				<?php
				
				if(get_sub_field('map_selection') == "on") { ?>
					<h3>Locations across Canada and the US:</h3>
					<p>Free Local Delivery And Pick Up</p>
					<h3 style="margin:1em 0 0;text-align: left;">Choose one of our locations:</h3>
					<div style="margin:5px 0 2em;width:30%;">
						<select name="" id="" class="form-control"  name="sort" onchange="javascript:location.href = this.options[this.selectedIndex].value">
							<option value="#">Select one</option>
							<option value="/abbotsford-langley">Abbotsford-Langley</option>
							<option value="/boise">Boise</option>
							<option value="/calgary">Calgary</option>
							<option value="/durham">Durham Region</option>
							<option value="/edmonton">Edmonton</option>
							<option value="/hamilton-oakville">Hamilton-Oakville</option>
							<option value="/kelowna">Kelowna - Penticton</option>
							<option value="/kitchener-waterloo">Kitchener-Waterloo</option>
							<option value="/lethbridge">Lethbridge</option>
							<option value="/london">London</option>
							<option value="/mississauga">Mississauga</option>
							<option value="/ottawa">Ottawa</option>
							<!-- option value="/saskatoon">Saskatoon</option -->
							<option value="/seattle">Seattle</option>
							<!-- option value="/st-johns">St. John's</option -->
							<option value="/surrey-delta">Surrey-Delta</option>
							<option value="/toronto">Toronto</option>
							<option value="/vancouver">Vancouver</option>
							<option value="/victoria">Victoria</option>
							<option value="/winnipeg">Winnipeg</option>
						</select>
					</div>
				<?php	echo do_shortcode( '[google_maps id="3562"]' );
				 }	?>
			</div>
		</div>

	<?php endif; ?>

<?php endwhile; ?>

<?php if($map_included != "Y") { ?>
<div class="row">
	<h3>Locations across Canada and the US:</h3>
	<p>Free Local Delivery And Pick Up</p>
	<h3 style="margin:1em 0 0;text-align: left;">Choose one of our locations:</h3>
	<div style="margin:5px 0 2em;width:30%;">
		<select name="" id="" class="form-control"  name="sort" onchange="javascript:location.href = this.options[this.selectedIndex].value">
			<option value="#">Select one</option>
			<option value="/abbotsford-langley">Abbotsford-Langley</option>
			<option value="/boise">Boise</option>
			<option value="/calgary">Calgary</option>
			<option value="/durham">Durham Region</option>
			<option value="/edmonton">Edmonton and Red Deer</option>
			<option value="/hamilton-oakville">Hamilton-Oakville</option>
			<option value="/kelowna">Kelowna - Penticton</option>
			<option value="/kitchener-waterloo">Kitchener-Waterloo</option>
			<option value="/lethbridge">Lethbridge</option>
			<option value="/london">London</option>
			<option value="/mississauga">Mississauga</option>
			<option value="/ottawa">Ottawa</option>
			<!-- option value="/saskatoon">Saskatoon</option -->
			<option value="/seattle">Seattle</option>
			<!-- option value="/st-johns">St. John's</option -->
			<option value="/surrey-delta">Surrey-Delta</option>
			<option value="/toronto">Toronto</option>
			<option value="/vancouver">Vancouver</option>
			<option value="/victoria">Victoria</option>
			<option value="/winnipeg">Winnipeg</option>
		</select>
	</div>
	<div class="mobile-map"><img src="/wp-content/uploads/2018/03/map-v7.png" alt=""></div>
	<div class="location-map">
	    <img src="/wp-content/uploads/2018/03/map-v7.png">
	    <a href="/Boise" style="top:59.5%;left:13.4%"><span>Boise</span></a>
	    <a href="/Calgary" style="top: 30.5%;left: 15.6%;"><span>Calgary</span></a>
	    <a href="/durham" style="top: 51.3%;left: 60.9%;"><span>Durham Region</span></a>
	    <a href="/Edmonton" style="top: 21%;left: 18%;"><span>Edmonton</span></a>
	    <a href="/Fraser-Valley" style="top: 38%;left: 5.8%;"><span>Fraser Valley</span></a>
	    <!-- a href="/Halifax" style="top: 56.7%;left: 82.5%;"><span>Halifax</span></a -->
	    <a href="/hamilton-oakville" style="top: 56.5%;left: 56.8%;"><span>Hamilton-Oakville</span></a>
	    <a href="/kitchener-waterloo" style="top: 55.8%;left: 55%;"><span>Kitchener-Waterloo</span></a>
	    <a href="/Lethbridge" style="top: 38.7%;left: 18.7%;"><span>Lethbridge</span></a>
	    <a href="/London" style="top: 59.6%;left: 53.1%;"><span>London</span></a>
	    <a href="/Mississauga" style="top: 53.2%;left: 58.1%;"><span>Mississauga</span></a>
	    <a href="/Okanagan" style="top: 34.1%;left: 10.3%;"><span>Okanagan</span></a>
	    <a href="/Ottawa" style="top: 41.2%;left: 67.6%;"><span>Ottawa</span></a>
	    <!-- a href="/Saskatoon" style="top: 29.1%;left: 24.5%;"><span>Saskatoon</span></a -->
	    <a href="/Seattle" style="top: 45%;left: 5.8%;"><span>Seattle</span></a>
	    <!-- a href="/St-Johns" style="top: 45.3%;left: 96.2%;"><span>St. John's</span></a -->
	    <a href="/Toronto" style="top: 52.2%;left: 59.4%;"><span>Toronto</span></a>
	    <!-- a href="/minneapolis-st-paul" style="top: 55.3%;left: 43%;"><span>Twin Cities</span></a -->
	    <a href="/Vancouver" style="top: 37%;left: 3.4%;"><span>Vancouver</span></a>
	    <a href="/Victoria" style="top: 42.4%;left: 3.7%;"><span>Victoria</span></a>
	    <a href="/Winnipeg" style="top: 35.6%;left: 38.1%;"><span>Winnipeg</span></a>
	</div>
</div>
<?php } ?>



        <?php endwhile; else: ?>

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>

        <?php endif; ?>


      </div>

      <?php get_sidebar( 'franchise' ); ?>

    </div>

<?php get_footer(); ?>