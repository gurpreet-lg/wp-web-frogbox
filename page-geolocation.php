<?php
/*
	Template Name: Geolocation Page Template
*/

?>

<?php get_header(); ?>

<div class="container container-inside-text">
<div class="row">

	<div class="col-md-9">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<div class="page-header">
				<h1><?php the_title(); ?></h1>
			</div>

			<?php the_content(); ?>

<script language="JavaScript" src="http://www.geoplugin.net/javascript.gp" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
    alert("Your location is: " + geoplugin_countryName() + ", " + geoplugin_region() + ", " + geoplugin_city());
    var country = geoplugin_countryName();
    $("#country").append("<option value='1' selected>"+country+"</option>");

    var zone = geoplugin_region();
    $("#zone").append("<option value='1' selected>"+zone+"</option>");

    var district = geoplugin_city();
    $("#district").append("<option value='1' selected>"+district+"</option>");
});
</script>
<label for="country">Country</label>
<select id="country" name="country" >
      <option value="" selected></option>
</select><br />
<label for="zone">Region</label>
<select id="zone" name="zone" >
      <option value="" selected></option>
</select><br />
<label for="district">City</label>
<select id="district" name="district" >
      <option value="" selected></option>
</select>


		<?php endwhile; else: ?>

			<div class="page-header">
				<h1>Oh no!</h1>
			</div>

			<p>No content is appearing for this page!</p>

		<?php endif; ?>

	</div>

	<?php get_sidebar("pricing"); ?>

</div>
</div>

<?php get_footer(); ?>