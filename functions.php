<?php

function remove_cssjs_ver( $src ) {
    if (strpos($src, 'frogbox-form')) {
        return $src;
    }

    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

function theme_styles() {

	//wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/css/bootstrap.css' );
	wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/css/bootstrap.min.css' );

	wp_enqueue_style( 'bootstrap_datepicker', get_template_directory_uri() . '/css/bootstrap-datepicker3.min.css' );
	wp_enqueue_style( 'bootstrap_datepicker_stand', get_template_directory_uri() . '/css/bootstrap-datepicker3.standalone.min.css' );
	//wp_enqueue_style( 'main_css', get_template_directory_uri() . '/style.css' );
      wp_enqueue_style( 'main_css', get_template_directory_uri() . '/style-v1.min.css' );


}
add_action( 'wp_enqueue_scripts', 'theme_styles' );

function theme_js() {

	global $wp_scripts;

	wp_register_script( 'html5_shiv', 'https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', '', '', false );
	wp_register_script( 'respond_js', 'https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', '', '', false );

	$wp_scripts->add_data( 'html5_shiv', 'conditional', 'lt IE 9' );
	$wp_scripts->add_data( 'respond_js', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'bootstrap__datepicker_js', get_template_directory_uri() . '/js/bootstrap-datepicker.min.js', array('jquery', 'bootstrap_js'), '', true );
	wp_enqueue_script( 'jsticky_js', get_template_directory_uri() . '/js/jquery.jsticky.js?ver=4.2.2', array('jquery', 'bootstrap_js', 'bootstrap__datepicker_js'), '', true );
	wp_enqueue_script( 'conformity_js', get_template_directory_uri() . '/js/conformity.js?ver=4.2.2', array('jquery', 'bootstrap_js', 'bootstrap__datepicker_js', 'jsticky_js'), '', true );
	wp_enqueue_script( 'theme_js', get_template_directory_uri() . '/js/theme-min-v1.js', array('jquery', 'bootstrap_js', 'bootstrap__datepicker_js', 'jsticky_js', 'conformity_js'), '', true );
        wp_enqueue_script( 'frogbox-custom-ga', get_template_directory_uri() . '/js/frogbox-custom-ga.js', array('jquery'), '', true);
}
add_action( 'wp_enqueue_scripts', 'theme_js' );

//add_filter( 'show_admin_bar', '__return_false' );

add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );

function register_theme_menus() {
	register_nav_menus(
		array(
			'header-menu'	=> __( 'Header Menu' ),
      'header-menu-cust' => __( 'Header Menu Custom' )
		)
	);
}
add_action( 'init', 'register_theme_menus' );


function create_widget( $name, $id, $description ) {

	register_sidebar(array(
		'name' => __( $name ),
		'id' => $id,
		'description' => __( $description ),
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));

}

//create_widget( 'Front Page Left', 'front-left', 'Displays on the left of the homepage' );
//create_widget( 'Front Page Center', 'front-center', 'Displays in the center of the homepage' );
//create_widget( 'Front Page Right', 'front-right', 'Displays on the right of the homepage' );


create_widget( 'Page Sidebar', 'page', 'Displays on the side of pages with a sidebar' );
//create_widget( 'About Sidebar', 'about', 'Displays on the side of pages with a sidebar' );
//create_widget( 'Blog Sidebar', 'blog', 'Displays on the side of pages in the blog section' );

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_filter( 'body_class','my_body_classes' );
function my_body_classes( $classes ) {
 
  
  if ( is_page( 'landing-page-template') || is_page( 'landing-page-template-v2') || is_page( 'location-promotionx-2017') || is_page( 'location-promotiony-2017') || is_page(5622) || is_page(5624) ) {
    $classes[] = 'fb-landing-page';
  }
  else {
    $classes[] = 'loading';
  }
   
  return $classes;
     
}


function custom_password_form() {
  global $post;
  if ( is_page('telus') || is_page('augusta-movers') ) {
    $text_main = "You're almost there! Enter the promocode below to unlock your savings:";
    $text_label ="Promocode";
  } else {
    $text_main = "This post is promocode protected. To view it please enter your promocode below:";
    $text_label ="Password";
  }
  $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
  $o = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post">
  <p>' . __( $text_main ) . '</p>
  <p><label for="' . $label . '">'. __( $text_label ) .' <input name="post_password" id="' . $label . '" type="password" size="20"></label> <input type="submit" name="Submit" value="' . esc_attr__( "Enter" ) . '"></p></form>';
  
  return $o;
}
add_filter( 'the_password_form', 'custom_password_form' );


function custom_query_shortcode($atts) {

   // EXAMPLE USAGE:
   // [loop_shortcode the_query="showposts=100&post_type=page&post_parent=453"]

   // Defaults
   extract(shortcode_atts(array(
      "the_query" => ''
   ), $atts));

   // de-funkify query
   $the_query = preg_replace('~&#x0*([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $the_query);
   $the_query = preg_replace('~&#0*([0-9]+);~e', 'chr(\\1)', $the_query);

   // query is made
   query_posts($the_query);

   // Reset and setup variables
   $output = '';
   $temp_title = '';
   $temp_link = '';
   $temp_ex = '';
   $temp_content = '';
   $temp_thumb = '';
   $temp_id = '';
   $temp_date ="";

   // the loop
   if (have_posts()) : while (have_posts()) : the_post();
      $temp_id = $post->ID;

      $temp_get_title = get_the_title($post->ID);
      $getlength = strlen($temp_get_title);
	$thelength = 70;
	$temp_title = substr($temp_get_title, 0, $thelength);
	if ($getlength > $thelength) $temp_dots = "...";

      $temp_link = get_permalink($post->ID);
      $temp_content = get_the_content($post->ID);
      $temp_date = get_the_date('Y-m-d', '', '');
      //$temp_ex = get_the_excerpt();
        if ( has_post_thumbnail() ) {
        $temp_thumb = get_the_post_thumbnail($post->ID, 'thumbnail-size');

        $thumbnail_id = get_post_thumbnail_id();
        $thumbnail_url = wp_get_attachment_image_src( $thumbnail_id, 'thumbnail', true );
        } else {
        $temp_thumb = "" ;
        $thumbnail_url = array("/wp-content/uploads/2015/07/widget_frog_bg.png");
        }


      // output all findings -
     /*
     $output .= "<div class='post-$temp_id' id='post-$temp_id'>
                <h3 class='entry-title'>
                    <a title='$temp_title' rel='bookmark' href='$temp_link'>$temp_title</a>
                </h3><!--BEGIN .entry-content-->
                <div class='entry-content'>
                    <div class='theExrp'>
                    <p>
                        <a href='$temp_link'>
                            $temp_thumb
                        </a>
                    </p>
                    <p>$temp_content

                    </p>
                    </div>
                    <div><a class='more-link' href='$temp_link'>read more...</a></div><!--END .entry-content-->
                </div><!--END .hentry-->
            </div>";
      */

      $output .= "<div id='post-$temp_id' class='post-$temp_id row' style='margin-bottom:20px;'>
      <div class='col-xs-4'><img src='$thumbnail_url[0]' style='width:80px;height:auto;'></div>
      <div class='col-xs-8'>
           <h4><a title='$temp_title' rel='bookmark' href='$temp_link'>$temp_title $temp_dots</a></h4>
            <!-- p>Date: $temp_date</p -->
      </div>
        </div>";
    endwhile; else:
      $output .= "not found.";
   endif;
   wp_reset_query();
   return $output;
}
add_shortcode("loop_shortcode", "custom_query_shortcode");



// add_action( 'vfb_override_email_1', 'vfb_action_override_email', 10, 5 );

// function vfb_action_override_email( $emails_to, $form_subject, $message, $headers, $attachments ){
//     // Checks radio button. Use Merge Tag to easily get $_POST id
//     if ( 'Calgary' == $_POST['vfb-field-10'] )
//         $emails_to = array( 'test1@test.com', 'stevefoldi@gmail.com' );
//     elseif ( 'Vancouver' == $_POST['vfb-field-10'] )
//         $emails_to = array( 'harry@test.com', 'steveotest778@gmail.com' );

//     // Send the mail
//     foreach ( $emails_to as $email ) {
//         wp_mail( $email, $form_subject, $message, $headers, $attachments );
//     }
// }


/*
That it is an implementation of the function money_format for the
platforms that do not it bear.

The function accepts to same string of format accepts for the
original function of the PHP.

(Sorry. my writing in English is very bad)

The function is tested using PHP 5.1.4 in Windows XP
and Apache WebServer.
*/
function money_format($format, $number)
{
    $regex  = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?'.
        '(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
    if (setlocale(LC_MONETARY, 0) == 'C') {
        setlocale(LC_MONETARY, '');
    }
    $locale = localeconv();
    preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
    foreach ($matches as $fmatch) {
        $value = floatval($number);
        $flags = array(
            'fillchar'  => preg_match('/\=(.)/', $fmatch[1], $match) ?
                $match[1] : ' ',
            'nogroup'   => preg_match('/\^/', $fmatch[1]) > 0,
            'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ?
                $match[0] : '+',
            'nosimbol'  => preg_match('/\!/', $fmatch[1]) > 0,
            'isleft'    => preg_match('/\-/', $fmatch[1]) > 0
        );
        $width      = trim($fmatch[2]) ? (int)$fmatch[2] : 0;
        $left       = trim($fmatch[3]) ? (int)$fmatch[3] : 0;
        $right      = trim($fmatch[4]) ? (int)$fmatch[4] : $locale['int_frac_digits'];
        $conversion = $fmatch[5];

        $positive = true;
        if ($value < 0) {
            $positive = false;
            $value  *= -1;
        }
        $letter = $positive ? 'p' : 'n';

        $prefix = $suffix = $cprefix = $csuffix = $signal = '';

        $signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
        switch (true) {
            case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
                $prefix = $signal;
                break;
            case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
                $suffix = $signal;
                break;
            case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
                $cprefix = $signal;
                break;
            case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
                $csuffix = $signal;
                break;
            case $flags['usesignal'] == '(':
            case $locale["{$letter}_sign_posn"] == 0:
                $prefix = '(';
                $suffix = ')';
                break;
        }
        if (!$flags['nosimbol']) {
            $currency = $cprefix .
                ($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']) .
                $csuffix;
        } else {
            $currency = '';
        }
        $space  = $locale["{$letter}_sep_by_space"] ? ' ' : '';

        $value = number_format($value, $right, $locale['mon_decimal_point'],
            $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
        $value = @explode($locale['mon_decimal_point'], $value);

        $n = strlen($prefix) + strlen($currency) + strlen($value[0]);
        if ($left > 0 && $left > $n) {
            $value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
        }
        $value = implode($locale['mon_decimal_point'], $value);
        if ($locale["{$letter}_cs_precedes"]) {
            $value = $prefix . $currency . $space . $value . $suffix;
        } else {
            $value = $prefix . $value . $space . $currency . $suffix;
        }
        if ($width > 0) {
            $value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ?
                STR_PAD_RIGHT : STR_PAD_LEFT);
        }

        $format = str_replace($fmatch[0], $value, $format);
    }
    return $format;
}



?>
