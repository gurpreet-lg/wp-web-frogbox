var $ = jQuery.noConflict();

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

// fun hamburger animation
(function() {

	"use strict";

	var toggles = document.querySelectorAll(".c-hamburger");

	for (var i = toggles.length - 1; i >= 0; i--) {
		var toggle = toggles[i];
		toggleHandler(toggle);
	};

	function toggleHandler(toggle) {
		toggle.addEventListener( "click", function(e) {
			e.preventDefault();
			(this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
		});
	}

})();


/////////////////////////////////////

jQuery(document).unbind('keydown').bind('keydown', function (event) {
	var doPrevent = false;
	if (event.keyCode === 8) {
		var d = event.srcElement || event.target;
		if ((d.tagName.toUpperCase() === 'INPUT' &&
		(
			d.type.toUpperCase() === 'TEXT' ||
			d.type.toUpperCase() === 'PASSWORD' ||
			d.type.toUpperCase() === 'FILE' ||
			d.type.toUpperCase() === 'SEARCH' ||
			d.type.toUpperCase() === 'EMAIL' ||
			d.type.toUpperCase() === 'NUMBER' ||
			d.type.toUpperCase() === 'DATE' )
		) ||
			d.tagName.toUpperCase() === 'TEXTAREA') {
			doPrevent = d.readOnly || d.disabled;
		}
		else {
			doPrevent = true;
		}
	}

	if (doPrevent) {
		event.preventDefault();
	}
});

///////////////////////////////////


jQuery(document).ready(function (){

	var boxHeight = jQuery("#order-summary").height();

	if (jQuery(window).width() >= 768) {

		jQuery("#header").sticky({
			zIndex:3,
			stopper: 300
		});

	jQuery(".sticky").sticky({
		topSpacing: 88,
		bottomSpacing: 160,
		zIndex:2,
		stopper: "#footer-line"
	});

	}

	// Initialize the plugin
	jQuery('.widget, .inner-border').conformity();

	// re-initialize the plugin on window resize
	jQuery(window).on("resize", function() {
		jQuery('.widget, #bundles-wrapper .inner-border, #bundles-products-wrapper .inner-border').conformity();
	});


	// Hide Header on on scroll down
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = jQuery('.navbar').outerHeight();

	jQuery(window).scroll(function(event){
		didScroll = true;
	});

	setInterval(function() {
		if (didScroll) {
			hasScrolled();
			didScroll = false;
		}
	}, 250);

	function hasScrolled() {
		var st = jQuery(this).scrollTop();

		// Make sure they scroll more than delta
		if(Math.abs(lastScrollTop - st) <= delta)
		return;

		// If they scrolled down and are past the navbar, add class .nav-up.
		// This is necessary so you never see what is "behind" the navbar.
		if (st > lastScrollTop && st > navbarHeight){
			// Scroll Down
			jQuery('.navbar').removeClass('nav-down').addClass('nav-up');
		} else {
			// Scroll Up
			if(st + jQuery(window).height() < jQuery(document).height()) {
				jQuery('.navbar').removeClass('nav-up').addClass('nav-down');
			}
		}

		lastScrollTop = st;
	}


	//slick slider stuff here
	var item_length = $('.slider-for > div').length - 1;
	var slider = $('.slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: false,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 3000,
		pauseOnFocus: false,
		pauseOnHover: true,
		speed: 500,
		slide: 'div',
		cssEase: 'cubic-bezier(0.600, -0.280, 0.735, 0.045)',
		asNavFor: '.slider-nav'
	}).on('afterChange', function( e, slick, currentSlide ) {
		if( currentSlide == 0 ) {
			slider.slick('slickPause');
			//console.log('paused');
		}
	});
	//slider.on('afterChange', function(event, slick, currentSlide, nextSlide){
	//check the length of total items in .slide container
	//if that number is the same with the number of the last slider
	//Then pause the slider
	//if( item_length === slider.slick('slickCurrentSlide') ){
	//slider.slick('slickPause');
	//};
	//});
	$('.slider-nav').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '.slider-for',
		dots: false,
		centerMode: false,
		arrows: false,
		infinite: false,
		autoplay: true,
		autoplaySpeed: 3000,
		pauseOnFocus: false,
		pauseOnHover: true,
		speed: 500,
		variableWidth: true,
		focusOnSelect: true,
		responsive: [
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					variableWidth: false,
					autoplay: false,
					//cssEase: 'linear',
					fade: false,
					infinite: true
				}
			}
		]
	});




});