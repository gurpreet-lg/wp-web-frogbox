<?php
/*
  Template Name: Flex Full Width Template
*/

?>
<?php get_header(); ?>

  <div class="container-fluid" style="max-width: 1280px;">
    <div class="row">

      <div class="col-md-12">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <?php 
            if(get_field('custom_page_title'))
            {
          ?>
            <div class="page-header">
              <h1><?php the_field('custom_page_title'); ?></h1>
            </div>
          <?php
            }
          ?>
          
          <?php
          /*
          *  Loop through a Flexible Content field and display it's content with different views for different layouts
          */
          while(the_flexible_field("content_builder")): ?>
              
              <?php if(get_row_layout() == "page_content_row"): // layout: Content ?>
                <div class="row">
                  <div class="col-sm-12"><?php the_sub_field("page_content"); ?></div>
                </div>
              <?php elseif(get_row_layout() == "page_image_row"): // layout: Content 
                $image = get_sub_field('page_image');
              ?>
                <div class="row">
                  <div class="col-sm-12"><img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>" /></div>
                </div>
              <?php elseif(get_row_layout() == "page_video_row"): // layout: Content ?>   
                <div class="row">
                  <div class="col-sm-12">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" src="<?php the_sub_field("page_video_url"); ?>"></iframe>
                    </div>
                  </div>
                </div>
              <?php endif; ?>

          <?php endwhile; ?>

          <?php the_content(); ?>

        <?php endwhile; else: ?>

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>

        <?php endif; ?>


      </div>

    </div>

<?php get_footer(); ?>