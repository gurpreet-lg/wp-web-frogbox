<!DOCTYPE html>
<html lang="en">
	<head>
    <script async src="//93206.tctm.co/t.js"></script>
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M338T5');</script>
<!-- End Google Tag Manager -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no" />
	<link rel="shortcut icon" href="<?php bloginfo('template_directory');?>/images/favicon.ico">

	<title>
	<?php wp_title( '|', true, 'right' ); ?>
	<?php bloginfo( 'name' ); ?>
	</title>

	<!-- <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'> -->
	<!-- script src="//use.typekit.net/lvy1waf.js"></script -->
	<!-- script>try{Typekit.load();}catch(e){}</script -->
	<script src="https://use.typekit.net/dir0ymy.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
	<!-- Add the slick-theme.css if you want default styling -->
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css"/>
	<?php wp_head(); ?>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script type="text/javascript">
	var boxHeight = '';
	</script>
	<script src="//cdn.optimizely.com/js/3049800342.js"></script>

<script src="//load.sumome.com/" data-sumo-site-id="f79e85392f1c8ec0ad1fb3cc30204f32050500eeec41b67f1a9652d23c4436e0" async="async"></script>

</head>
	<body <?php body_class(); ?>>
    <!--ClickCease -->
    <script type="text/javascript">var script = document.createElement('script');script.async = true; script.type = 'text/javascript';var target = 'https://www.clickcease.com/monitor/stat.js';script.src = target;var elem = document.head;elem.appendChild(script);</script>
    <!--End ClickCease -->

    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M338T5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php if ( !get_field('hide_header') ) { ?>
	<div class="navbar nav-down navbar-inverse" role="navigation">
		<div class="container-fluid" style="max-width: 1280px;">
			<div class="navbar-header">
				<button type="button" class="c-hamburger c-hamburger--htx  navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="hamburger-menu1 sr-only1">Toggle navigation</span>
					<!-- <span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span> -->
				</button>
				<div class="navbar-brand">
				<a href="<?php bloginfo( 'url' ); ?>"><img src="<?php bloginfo('template_url') ?>/images/logo-frogbox-2.png" alt="<?php bloginfo('description'); ?>" title="<?php bloginfo('name'); ?>" class="large" /></a><a href="<?php bloginfo( 'url' ); ?>"><img src="<?php bloginfo('template_url') ?>/images/frogbox-logo-sm.png" alt="<?php bloginfo('description'); ?>" title="<?php bloginfo('name'); ?>" class="small" /></a>
				</div>
			</div>

			<div class="navbar-collapse collapse navbar-right">

			<!-- <div class="nav-contact hidden-xs">Need help? Call us at <span><a href="tel:+1-877-376-4269" style="cursor: text;"><img src="<?php //bloginfo('template_url') ?>/images/frogbox-number.png" alt="1-877-frogbox"></a></span></div> -->

			<style>
			  .hoverable {
			    cursor:default;
			    color:#000;
			    text-decoration:none;
			  }
			  .hoverable .hover {
			    display:none;
			  }
			  .hoverable:hover .normal {
			    display:none;
			  }
			  .hoverable:hover .hover {
			    display:inline;  /* CHANGE IF FOR BLOCK ELEMENTS 1 */
			  }
			</style>
			<div class="nav-contact hidden-xs">Need help? Call us at
			<a href="tel:+1-877-376-4269" style="cursor: text;font-size: 18px;color: #939597;" class="hoverable">
			<span class="normal">1-877-<span style="color: #8CC640;">FROG</span><span style="color: #00a5c7;">BOX</span></span>
			<span class="hover">1-877-376-4269</span>
			</a>
			</div>

			<?php
			$args = array(
			'menu'        => 'header-menu',
			'menu_class'  => 'nav navbar-nav',
			'container'   => 'false'
			);
			wp_nav_menu( $args );
			?>

			</div><!--/.navbar-collapse -->

		</div>
	</div>
	<?php } ?>