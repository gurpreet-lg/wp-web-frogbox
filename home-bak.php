<?php get_header(); ?>

  <div class="container">
    <div class="row">

      <div class="col-xs-12">

        <div class="page-header">
          <h1>FrogBlog</h1>
        </div>
        <?php $the_query = new WP_Query( 'category_name=blog' ); ?>
        <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

          <?php
              if ( has_post_thumbnail() ) {
              $thumbnail_id = get_post_thumbnail_id();
              $thumbnail_url = wp_get_attachment_image_src( $thumbnail_id, 'thumbnail-size', true );
              $thumbnail_meta = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true);
              } else {
                $thumbnail_url = array("/wp-content/uploads/2015/07/widget_frog_bg.png");
              }
            ?>

          <article class="post row">
            <div class="col-sm-3 text-center"><img src="<?php echo $thumbnail_url[0]; ?>" alt="<?php echo $thumbnail_meta; ?>"></div>
            <div class="col-sm-9">
            <h2 style="font-size:2em;margin:0;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <p style="font-weight:bold;font-size:1.2em;"><em>
              <?php echo the_time('l, F jS, Y');?>
              <br><?php the_category( ', ' ); ?>
            </em></p>
            <p><?php the_excerpt(); ?></p>
            </div>
          </article>
          <hr>

        <?php endwhile; ?>
        <div class="row">
          <?php next_posts_link( '<div class="col-sm-3"><div class="btn btn-info-previous">Previous</div></div>' ); ?>
          <?php previous_posts_link( '<div class="col-sm-3 col-sm-offset-6"><div class="btn btn-info">Next</div></div>' ); ?>
        </div>

        <?php else: ?>

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>

        <?php endif; ?>


      </div>

      <?php //get_sidebar( 'blog' ); ?>

    </div>

<?php get_footer(); ?>