<?php
/*
  Template Name: Partner Flex Sidebar Template
*/

?>

<?php get_header(); ?>
<style type="text/css">
.content-block {
  margin-top: 20px;
  margin-bottom: 20px;
}
</style>
  <div class="container-fluid container-inside-text">
	
  <?php if(get_field('partner_custom_page_title')) { ?>
    <div class="row">
      <div class="col-sm-12 page-header"><h1><?php the_field('partner_custom_page_title'); ?></h1></div>
    </div>
  <?php } ?>

    <div class="row">

      <div class="col-xs-12 col-sm-9">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        
        <?php
          /*
          *  Loop through a Flexible Content field and display it's content with different views for different layouts
          */
          while(the_flexible_field("partner_content_flex")): ?>
              
              <?php if(get_row_layout() == "partner_content_row"): // layout: Content ?>
                <div class="row content-block">
                  <div class="col-sm-12" style="padding-left: 0;padding-right: 0;"><?php the_sub_field("partner_content_block"); ?></div>
                </div>
              <?php elseif(get_row_layout() == "partner_repeater_row"): // layout: Content ?>
                
                <?php if( have_rows('partner_repeater_block') ): ?>
                  <?php while( have_rows('partner_repeater_block') ): the_row(); 
                  // vars
                  $image = get_sub_field('repeater_image');
                  $title = get_sub_field('repeater_title');
                  $content = get_sub_field('repeater_content');
                  $link = get_sub_field('repeater_url');
                  ?>
                  
                    <div class="post row">
                      <div class="col-sm-3 text-center"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></div>
                      <div class="col-sm-9">
                        <h2 style="font-size:2em;margin:0;"><?php echo $title; ?></h2>
                        <?php if( $link ): ?>
                          <p><a href="<?php echo $link; ?>" target="_blank"><?php echo $link; ?></a></p>
                        <?php endif; ?>
                        <?php echo $content; ?>
                      </div>
                    </div>

                    <hr>

                  <?php endwhile; ?>
                <?php endif; ?>

              <?php endif; ?>

          <?php endwhile; ?>

          <?php the_content(); ?>

        <?php endwhile; else: ?>

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>

        <?php endif; ?>


      </div>

      <?php get_sidebar(); ?>

    </div>

<?php get_footer(); ?>