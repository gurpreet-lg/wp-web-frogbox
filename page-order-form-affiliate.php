<?php
/*
  Template Name: Order form template for affiliates
*/

?>

<?php 
global $post;
get_header();
?>

  <div class="container-fluid container-inside-text">

    <div class="row">
      
      <?php if ( ! post_password_required( $post ) ) { ?>

      <div class="col-xs-12 col-sm-9">
      
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <?php the_content(); ?>

        <?php endwhile; else: ?>

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>

        <?php endif; ?>


      </div>
      
      <?php get_sidebar("pricing-affiliate"); ?>

      <?php 
        } else {
          echo get_the_password_form();
        }
      ?>
    </div>

<?php get_footer(); ?>
