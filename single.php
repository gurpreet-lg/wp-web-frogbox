<?php get_header(); ?>

  <div class="container">
    <div class="row">

      <div class="col-xs-12">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <div class="page-header">
            <style type="text/css">
            .field-franchises-links {
              display:none;
            }
            </style>
            <?php
              $thumbnail_id = get_post_thumbnail_id();
              $thumbnail_url = wp_get_attachment_image_src( $thumbnail_id, 'thumbnail-size', false );
              $thumbnail_meta = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true);
            ?>
            <p class="featured-image" style="text-align: center;margin-top: 20px;"><img src="<?php echo $thumbnail_url[0]; ?>" alt="<?php echo $thumbnail_meta; ?>"></p>

            <h1 style="font-size:3em;text-align:center;"><?php the_title(); ?></h1>
            <p style="text-align:center;font-weight:bold;font-size:1.2em;"><em>
              <?php echo the_time('l, F jS, Y');?>
              <br><?php the_category( ', ' ); ?>
              <!-- <a href="<?php comments_link(); ?>"><?php comments_number(); ?></a> -->
            </em></p>
          </div>

          <?php the_content(); ?>

          <!-- <hr> -->

          <?php //comments_template(); ?>

        <?php endwhile; else: ?>

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>

        <?php endif; ?>


      </div>

      <?php //get_sidebar( 'blog' ); ?>

    </div>

<?php get_footer(); ?>