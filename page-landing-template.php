<?php
/*
  Template Name: Landing Page Template
*/
?>
<?php get_header(); ?>

<style type="text/css">
body.fb-landing-page {
  padding-top: 0;
}

.row-standard {
  margin-top: 80px;
  margin-bottom: 80px;
}
@media (max-width: 768px) {
  .row-standard {
    margin-top: 20px;
    margin-bottom: 20px;
  }
}
.row-custom-1 {
  margin-top: 40px;
  margin-bottom: 80px;
}
@media (max-width: 768px) {
  .row-custom-1 {
    margin-top: 20px;
    margin-bottom: 20px;
  }
}
.no-padding-x {
  padding-left: 0;
  padding-right: 0;
}

.lp-navbar {
  background-color: #fff;
  height: auto;
  position: relative;
  /*top: 0;
  transition: top .2s ease-in-out;*/
  width: 100%;
  min-height: inherit;
  margin-bottom: 0;
  border-color: transparent;
  border-bottom: 1px solid #e6e7e8;
  z-index: 999;
}

.lp-logo {
  padding: 20px 0 10px;
  text-align: center;
}
.lp-logo .large {
  display: inline-block;
}
.lp-logo .small {
  display: none;
}
.lp-location {
  text-align: center;
  padding: 0 0 4px;
}
.lp-location h3 {
  margin: 0;
  font-weight: inherit;
  line-height: inherit;
}
.call-frogbox {
  padding: 0 0 4px;
  text-align: center;
}
<?php //echo is_page(); ?>
<?php if ( is_page(5622) ) {
$bg_image = "/wp-content/uploads/2017/07/Frogbox-Moving-Boxes-and-Supplies-Free-Delivery.jpg";
$position_vert = "top";
}
elseif ( is_page(5624) ) {
$bg_image = "/wp-content/uploads/2017/07/Frogbox-Moving-Boxes-and-Supplies-Free-Delivery1.jpg";
$position_vert = "center";
}
?>
.lp-block-hero-bg {
  background: url('<?php echo $bg_image; ?>') no-repeat <?php echo $position_vert; ?> center;
  background-size: cover;
  height: 60vh;
  min-height: 680px;
}
@media (max-width: 420px) {
  .lp-block-hero-bg {
    height: 67vh;
    min-height: initial;
  }
}

.lp-block-hero {
  height: 100%;
  background: rgba(0,0,0,0.6);
}
.lp-block-hero h2 {
  margin-top: 0;
  font-size: 38px;
}
.lp-block-hero h3 {
  padding-bottom: 20px;
  font-size: 30px;
  color:#00a5c7;
}
@media (max-width: 600px) {
  .lp-block-hero h2 {
    margin-top: 0;
    font-size: 28px;
  }
  .lp-block-hero h3 {
    padding-bottom: 10px;
    font-size: 20px;
    color:#00a5c7;
  }
}
.lp-block-hero .infusion-form {}

.hoverable {
  cursor:default;
  color:#000;
  text-decoration:none;
}
.hoverable .hover {
  display:none;
}
.hoverable:hover .normal {
  display:none;
}
.hoverable:hover .hover {
  display:inline;  /* CHANGE IF FOR BLOCK ELEMENTS 1 */
}

.element-a {
  position: relative;
  top: 8vh;
  /*top: 50%;
  transform: translateY(-50%);*/
  width: 100%;
  max-width: 600px;
  margin: 0 auto;
  text-align: center;
}
@media (max-width: 950px) {
  .element-a {
    top: 3vh;
    padding: 0 10px;
  }
}
.element-b {
  position: relative;
  top: 8vh;
  /*top: 50%;
  transform: translateY(-50%);*/
  width: 100%;
  max-width: 600px;
  margin: 0 auto;
  text-align: center;
}
@media (max-width: 768px) {
  .element-b {
    padding: 0 10px;
  }
}

.btn-primary,
.btn-primary:hover,
.btn-primary:focus {
  width: 100%;
  height: auto;
  background-color: #f77f2f;
  border-color: #f77f2f;
  padding: .6em 0;
  font-family: inherit;
  font-size: 34px;
  font-weight: 600;
  line-height: 1;
  color: #fff;
}
@media (max-width: 600px) {
  .btn-primary,
  .btn-primary:hover,
  .btn-primary:focus {
    font-size: 28px;
  }
}
@media (max-width: 440px) {
  .btn-primary,
  .btn-primary:hover,
  .btn-primary:focus {
    font-size: 20px;
  }
}
</style>

<div class="lp-navbar navbar1" role="navigation">
  <div class="container-fluid" style="max-width: 1280px;">
    <div class="lp-logo"><a href="/"><img src="/wp-content/themes/frogbox/images/logo-frogbox-2.png" alt="From One Pad To Another" title="Frogbox" class="large" /><img src="/wp-content/themes/frogbox/images/frogbox-logo-sm.png" alt="From One Pad To Another" title="Frogbox" class="small" /></a></div>
    <div class="lp-location"><h3>Vancouver</h3></div>
    <div class="call-frogbox">
      <a href="tel:+1-877-376-4269" style="cursor: text;font-size: 18px;color: #939597;" class="hoverable">
      <span class="normal">1-877-<span style="color: #8CC640;">FROG</span><span style="color: #00a5c7;">BOX</span></span>
      <span class="hover">1-877-376-4269</span>
      </a>
    </div>
  </div>
</div>
<style type="text/css">
.infusion-field {
  padding: 0 0 10px;
}
@media (max-width: 768px) {
  .infusion-field input[type="text"] {
    padding: 10px;
  }
}
.infusion-submit {
  padding: 10px 0;
}
.infusion-form .btn-primary,
.infusion-form .btn-primary:hover,
.infusion-form .btn-primary:focus {
  width: 100%;
  height: auto;
  background-color: #f77f2f;
  border-color: #f77f2f;
  /*padding: 1.45em 6em;*/
  padding: 0.6em 0;
  font-family: inherit;
  /*font-size: inherit;*/
  font-size: 34px;
  font-weight: 600;
  /*line-height: inherit;*/
  line-height: 1;
  color: #fff;
  // text-transform: uppercase;
}
@media (max-width: 600px) {
  .infusion-form .btn-primary,
  .infusion-form .btn-primary:hover,
  .infusion-form .btn-primary:focus {
    font-size: 28px;
  }
}
@media (max-width: 440px) {
  .infusion-form .btn-primary,
  .infusion-form .btn-primary:hover,
  .infusion-form .btn-primary:focus {
    font-size: 20px;
  }
}
.infusion-form .btn-primary:after {
  font-family: FontAwesome;
  content: "\f105";
  display: inline-block;
  /* font-size: 1.4em; */
  font-size: 1.2em;
  border: 1px solid #fff;
  border-radius: 30px;
  /* padding: 2px 14px 4px; */
  padding: 1px 13px 1px 15px;
  margin-left: 14px;
  /* vertical-align: middle; */
}
@media (max-width: 600px) {
  .infusion-form .btn-primary:after {
    padding: 1px 10px 1px 13px;
  }
}
@media (max-width: 400px) {
  .infusion-form .btn-primary:after {
    padding: 1px 7px 1px 10px;
  }
}
.infusion-form .btn-primary:hover:after {
  background-color: #fff;
  border: 1px solid #fff;
  color: #f77f2f;
}

.burst-container {
  display: inline-block;
  position: absolute;
  top: -4vh;
  right: -200px;
}
.burst-msg {
  display: inline-block;
  border-radius: 80px;
  border: 6px solid #fff;
  padding: 26px;
  background: #8cc640;
  color: #fff;
  text-align: center;
  font-size: 22px;
}
@media (max-width: 1040px) {
  .burst-container {
    right: -160px;
  }
}
@media (max-width: 950px) {
  .burst-container {
    position: relative;
    top: 0;
    right: 0;
    left: 0;
    width: 100%;
    margin: 0 auto;
    text-align: center;
  }
}
</style>
<?php
  if ( is_page(5624) ) {
    $button_text = "Book Now & Get Packing!";
    $class_element = "element-a";
  }
  elseif ( is_page(5622) ) {
    $button_text = "Order now & Save $15";
    $class_element = "element-b";
  }
?>
<div class="lp-block-hero-bg">
  <div class="lp-block-hero">
    <div class="<?php echo $class_element; ?>">
      <?php
        if ( is_page(5624) ) {
      ?>
      <div class="burst-container"><div class="burst-msg">Order now<br>&<br>Save $15</div></div>
      <?php } ?>
      <h2 style="">Save time & money by renting<br>
      reusable moving boxes</h2>
      <h3 style="">Free Delivery & Pickup in Vancouver and Lower mainland</h3>
      <form accept-charset="UTF-8" action="https://vq188.infusionsoft.com/app/form/process/c0af159215da92206244d970bd9fc641" class="infusion-form" method="POST">
        <input name="inf_form_xid" type="hidden" value="c0af159215da92206244d970bd9fc641" />
        <input name="inf_form_name" type="hidden" value="Web Form submitted" />
        <input name="infusionsoft_version" type="hidden" value="1.67.0.54" />
        <div class="infusion-field">
          <!-- <label for="inf_field_FirstName">First Name *</label> -->
          <input class="infusion-field-input-container" id="inf_field_FirstName" name="inf_field_FirstName" type="text" placeholder="First Name *" />
        </div>
        <div class="infusion-field">
          <!-- <label for="inf_field_Email">Email *</label> -->
          <input class="infusion-field-input-container" id="inf_field_Email" name="inf_field_Email" type="text" placeholder="Email *" />
        </div>
        <div class="infusion-submit">
          <button class="btn btn-primary" type="submit"><?php echo $button_text; ?></button>
          <!-- <input type="button" value="Book Now & Get Packing!" class="btn btn-primary" /> -->
        </div>
      </form>
      <script type="text/javascript" src="https://vq188.infusionsoft.com/app/webTracking/getTrackingCode"></script>
      <!-- <br><br>
      <div style="margin: 30px 0 20px;"><input id="vfb-field-1" class="vfb-form-control" placeholder="Enter email..." required="required" type="text" name="vfb-field-1" value="" data-vfb-id="6434"></div>
      <a class="btn btn-default" href="/pricing">Book Now & Get Packing!</a> -->
      <!-- <button id="vfb-field-9" class=" btn btn-primary" placeholder="" type="submit" name="_vfb-submit">Book Now & Get Moving!</button> -->
    </div>
  </div>
</div>

<style type="text/css">
.text-block {
  background: #8cc640;
  font-size: 40px;
  font-weight: 700;
  color: #fff;
  text-transform: uppercase;
  text-align: center;
}
.text-top {
  padding: 30px 20px 20px;
}
.text-bottom {
  padding: 20px 20px 20px;
}
.text-smaller {
  font-size: 30px;
}
@media (max-width: 420px) {
  .text-block {
    font-size: 26px;
  }
  .text-smaller {
    font-size: 20px;
  }
}

@media (max-width: 768px) {
  .text-top {
    padding: 10px;
  }
  .text-bottom {
    padding: 10px;
  }
  .the-chart table tbody tr td {
    font-size: 14px;
  }
}
</style>

<div class="container-fluid container-inside-text">
  
  <div class="row row-standard">
    <div class="col-sm-12 no-padding-x">
      
      <div style="width: 100%;max-width: 1000px;margin: 0 auto;text-align: center;">
        <div class="text-block text-top">
          Frogboxes VS Cardboard Moving Boxes
        </div>
        
        <div class="the-chart table-responsive">
          <table class="table table-bordered">
          <thead>
            <tr>
              <th></th>
              <th style="text-align: center;vertical-align: middle;font-size: 20px;"><span style="color: #8CC640;">FROG</span><span style="color: #00a5c7;">BOX</span></th>
              <th style="text-align: center;vertical-align: middle;">Cardboard<br>Boxes</th>
            </tr>
          </thead>
          <tbody>
            <tr style="background: #00a5c7;color: #fff;font-size: 20px;">
              <td>CONVENIENT</td>
              <td><i class="fa fa-check" aria-hidden="true" style="color: #8cc640;font-size: 25px;"></i></td>
              <td>No</td>
            </tr>
            <tr style="font-size: 20px;">
              <td>Clean, reusable plastic moving boxes</td>
              <td><i class="fa fa-check" aria-hidden="true" style="color: #8cc640;font-size: 25px;"></i></td>
              <td>No</td>
            </tr>
            <tr style="font-size: 20px;">
              <td>Delivered ready to pack</td>
              <td><i class="fa fa-check" aria-hidden="true" style="color: #8cc640;font-size: 25px;"></i></td>
              <td>No</td>
            </tr>
            <tr style="font-size: 20px;">
              <td>Picked up at your new home</td>
              <td><i class="fa fa-check" aria-hidden="true" style="color: #8cc640;font-size: 25px;"></i></td>
              <td>No</td>
            </tr>
            <tr style="font-size: 20px;">
              <td>Sturdy, stackable, water resistant</td>
              <td><i class="fa fa-check" aria-hidden="true" style="color: #8cc640;font-size: 25px;"></i></td>
              <td>No</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr style="background: #00a5c7;color: #fff;font-size: 20px;">
              <td>SAVE TIME AND $$$</td>
              <td><i class="fa fa-check" aria-hidden="true" style="color: #8cc640;font-size: 25px;"></i></td>
              <td>No</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr style="background: #00a5c7;color: #fff;font-size: 20px;">
              <td>ECO-FRIENDLY - No waste!</td>
              <td><i class="fa fa-check" aria-hidden="true" style="color: #8cc640;font-size: 25px;"></i></td>
              <td>No!</td>
            </tr>
          </tbody>
          </table>
        </div>

        <div class="text-block text-bottom">
          Rent Frogboxes - <span class="text-smaller" style="">make your move easy</span>
        </div>
      </div>

    </div>
  </div>
  
  <hr style="clear:both;">

  <div class="row row-standard">
    <div class="col-sm-4" style="margin: 0 0 20px;text-align: center;">
      <img src="/wp-content/uploads/2015/07/inner-step1.jpg" alt="Step 1" />
      <h4 style="font-size: 1.2em; line-height: 1.4;text-align: left;">1. We Drop Off Your Frogbox Moving Boxes</h4>
      <p style="text-align: left;">No need to drive to pick up expensive cardboard moving boxes. Frogboxes are dropped off where you want them by our friendly team.</p>
    </div>
    <div class="col-sm-4" style="margin: 0 0 20px;text-align: center;">
      <img src="/wp-content/uploads/2015/07/inner-step2.jpg" alt="Step 2" />
      <h4 style="font-size: 1.2em; line-height: 1.4;text-align: left;">2. You Pack Your Stuff in Frogbox Plastic Moving Boxes and Move to Your New Pad</h4>
      <p style="text-align: left;">Our eco-friendly plastic moving boxes make packing quicker and easier. No more building and taping cardboard moving boxes. Frogboxes are roomy and don’t collapse.</p>
      <p style="text-align: left;">Frogboxes are much easier to handle than cardboard. They stack perfectly, protect your valuables, and are water-resistant and have ergonomic handles. Frogbox also offers dollies to make your move even easier.</p>
    </div>
    <div class="col-sm-4" style="margin: 0 0 20px;text-align: center;">
      <img src="/wp-content/uploads/2015/07/inner-step3.jpg" alt="Step 3" />
      <h4 style="font-size: 1.2em; line-height: 1.4;text-align: left;">3. We Pick Up Your Frogbox Moving Boxes</h4>
      <p style="text-align: left;">Once you are moved in and unpacked, one of our Frogbox trucks picks up the plastic moving boxes at your new place. This means you can spend your time enjoying your new house, not breaking down cardboard boxes and figuring out what to do with them.</p>
    </div>
  </div>
  
  <hr />

  <div class="row row-standard">  
    <div class="col-sm-12" style="margin: 0 0 20px; padding: 0;text-align: center;">
      <h3>What Fits in a Frog<span style="color: #00a5c7;">box</span>?</h3>
      <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/USglAkr55xs?VQ=HD720&amp;rel=0"></iframe>
      </div>
    </div>
  </div>

  <hr />
  <style type="text/css">
  @media (max-width: 768px) {
    .the-rating img {
      width: 280px;
    }
  }
  @media (max-width: 360px) {
    .the-rating img {
      width: 240px;
    }
  }
  </style>
  <div class="row row-standard">  
    <div class="col-sm-12" style="margin: 0 0 20px; padding: 0;">
      <div style="width: 100%;max-width: 600px;margin: 0 auto;text-align: center;">
        <div style="margin:20px 0 0;"><img src="/wp-content/uploads/2017/07/place-female-head.png" alt=""></div>
        <div style="margin: 20px 0;text-align: left;">
          <p>The man who delivered our boxes was so friendly and progressional - the man who co-ordinated everything on the phone and email was also fabulous I cannot think of anything better !</p>
          <h4 style="text-align:right;">- Samantha from Vancouver, B.C.</h4>
        </div>
        <div class="the-rating" style="margin:0 0 40px;"><nobr><img src="/wp-content/uploads/2017/07/place-five-stars-orange-fill.png" alt="" style="padding: 0 10px 0 0;"><span style="display: inline-block;text-align:center;font-size: 36px;color: #f77f2f;vertical-align: baseline;">5/5</span></nobr></div>
        <a class="btn btn-primary" href="/pricing">Book Now & Get Packing!</a>
        <!-- <button id="vfb-field-9" class=" btn btn-primary" placeholder="" type="submit" name="_vfb-submit">Book Now & Get Moving!</button> -->
      </div>
    </div>
  </div>


  <hr id="footer-line">

  <footer id="footer">
    <div class="row">
      <div class="col-sm-12">
        <!-- <div class="nav-contact" style="text-align: center;">Need help? Call us at
        <a href="tel:+1-877-376-4269" style="cursor: text;font-size: 18px;color: #939597;" class="hoverable">
        <span class="normal">1-877-<span style="color: #8CC640;">FROG</span><span style="color: #00a5c7;">BOX</span></span>
        <span class="hover">1-877-376-4269</span>
        </a>
        </div> -->
        <p style="font-size:14px;">&copy; Frogbox 2017</p>
      </div>
    </div>
  </footer>

<?php get_footer(); ?>