<?php
/*
  Template Name: CAA Full Width Template
*/

require_once(WP_PLUGIN_DIR . '/frogbox-form/frogbox-caa.inc');

$error = '';
if (!empty($_REQUEST['membership_number'])) {
  $caaID = $_REQUEST['membership_number'];

  // if membership # is valid, redirect to order page, if not show an error
  if (_ff_validate_caa($caaID)) {
    wp_redirect(site_url() . '/order?caa=' . $caaID);
    die;
  }
  else {
    $error = '<span class="error">The 16-digit membership number you\'ve given doesn\'t match CAA\'s records. 
			Please try again or <a href="/order">place an order</a> without one.</span>';
  }

}

?>
<?php get_header(); ?>

  <div class="container-fluid" style="max-width: 1280px;">
    <div class="row">

      <div class="col-md-12">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <!-- <div class="page-header">
            <h1><?php //the_title(); ?></h1>
          </div> -->
          
          <?php the_content(); ?>
          <style type="text/css">
          .button-area {
            width: 45%;
            margin: 0 auto;
            text-align: center;
          }
          @media (max-width: 768px) {
            .button-area {
              width: 84%;
            }
          }
          </style>
          <form method="post">
            <div class="row" style="margin-bottom: 20px;">
              <div class="col-sm-6 col-sm-offset-3">
                
                <div class="row">
                  <div class="col-sm-12">
                    <?php if (!empty($error)) { echo $error; } ?>
                    <label for="membership_number">Enter your 16-digit CAA Membership Number:</label>
                    <input type="text" id="membership_number" name="membership_number" required="" aria-required="true">
                  </div>
                </div>

                
              </div>
            </div>
            <div class="row">
              <div class="button-area">
                  <button id="caa-continue" class="btn btn-default">Continue</button>
              </div>
            </div>
          </form>

        <?php endwhile; else: ?>

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>

        <?php endif; ?>


      </div>

    </div>

<?php get_footer(); ?>
