<div class="col-xs-12 col-sm-3 sidebar">

<?php
/*
*  Loop through a Flexible Content field and display it's content with different views for different layouts
*/

while(the_flexible_field("content")): ?>

	<?php if(get_row_layout() == "sidebar"): // layout: Content ?>

		<div style="padding: 10px 20px 20px;">
			<h4>Question?</h4>
			<?php the_sub_field("question"); ?>

			<h4>Location:</h4>
			<?php the_sub_field("location"); ?>

			<h4>Email:</h4>
			<a href="/contact/<?php the_sub_field("contact_drop_down_filter"); ?>"><?php the_sub_field("email"); ?></a>

			<h4>Phone:</h4>
			<a href="tel:+<?php the_sub_field("phone"); ?>"><?php the_sub_field("phone"); ?></a>
			
			<?php if(get_sub_field('social')) { ?>
			<h4>Connect with us:</h4>
			<?php the_sub_field("social"); ?>
			<?php } ?>

			<?php if(get_sub_field('friends_partners_link')) { ?>
			<h4>Connect with our Partners:</h4>
			<div style="margin:20px 0;"><a href="<?php the_sub_field("friends_partners_link"); ?>" style="display:inline-block;padding-right: 5px;">Friends<br>& Partners</a><img src="/wp-content/uploads/2015/07/widget_frog_bg.png" alt="" style="display:inline-block;width:120px;"></div>
			<?php } ?>
		</div>

	<?php endif; ?>

<?php endwhile; ?>

</div>