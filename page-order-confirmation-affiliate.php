<?php
/*
  Template Name: Order form confirmation for affiliates
*/
?>
<?php get_header(); ?>

  <div class="container-fluid container-inside-text">

    <div class="row">

      <div class="col-xs-12 col-sm-9">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <!-- <div class="page-header">
            <h1><?php the_title(); ?></h1>
          </div> -->

          <?php the_content(); ?>



        <?php endwhile; else: ?>

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>

        <?php endif; ?>


      </div>

      <?php get_sidebar("pricing-affiliate"); // necessary for displaying form correctly on validation error ?>
      <?php //get_sidebar("order-confirm"); ?>

    </div>
<?php get_footer(); ?>
