<?php
/*
	Template Name: Blog
*/
?>
<?php get_header(); ?>

<div class="container">
	<div class="row">

      <div class="col-xs-12">
      <div class="page-header">
          <h1>Frog Blog</h1>
        </div>

		<?php // Display blog posts on any page @ http://m0n.co/l
			$temp = $wp_query; $wp_query= null;
			$wp_query = new WP_Query(); $wp_query->query('category_name=blog'.'&showposts=10' . '&paged='.$paged);
			while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

		<!-- <h2><a href="<?php the_permalink(); ?>" title="Read more"><?php the_title(); ?></a></h2> -->
		<?php //the_excerpt(); ?>
		<?php
			if ( has_post_thumbnail() ) {
				$thumbnail_id = get_post_thumbnail_id();
				$thumbnail_url = wp_get_attachment_image_src( $thumbnail_id, 'thumbnail-size', true );
				$thumbnail_meta = get_post_meta( $thumbnail_id, '_wp_attachment_image_alt', true);
			} else {
				$thumbnail_url = array("/wp-content/uploads/2015/07/widget_frog_bg.png");
			}
            ?>

		<article class="post row">
			<div class="col-sm-3 text-center"><img src="<?php echo $thumbnail_url[0]; ?>" alt="<?php echo $thumbnail_meta; ?>"></div>
			<div class="col-sm-9">
				<h2 style="font-size:2em;margin:0;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p style="font-weight:bold;font-size:1.2em;"><em>
				<?php //echo the_time('l, F jS, Y');?>
				<br><?php the_category( ', ' ); ?>
				</em></p>
				<p><?php the_excerpt(); ?></p>
			</div>
		</article>
		<hr>

		<?php endwhile; ?>

		<?php if ($paged > 1) { ?>
		<div class="row">
			<?php next_posts_link( '<div class="col-sm-3"><div class="btn btn-info-previous">Previous</div></div>' ); ?>
			<?php previous_posts_link( '<div class="col-sm-3 col-sm-offset-6"><div class="btn btn-info">Next</div></div>' ); ?>
		</div>

		<?php } else { ?>
		<div class="row">
			<?php next_posts_link( '<div class="col-sm-3"><div class="btn btn-info-previous">Previous</div></div>' ); ?>
		</div>

		<?php } ?>

		<?php wp_reset_postdata(); ?>

	</div>

	</div>
</div>

<?php get_footer(); ?>