<div id="mobile_menu" class="col-xs-12 col-sm-3 sidebar-mobile clearfix">

	<div id="order-summary" class="sticky sidebar" style="position: relative; top: 0px; width: auto;">
		<div id="mobile-summary" class="group-hidden">
			<button class="c-hamburger c-hamburger--htx reveal-trigger"  href="#mobile_menu"><span>toggle menu</span></button>
			<span id="mobile-price-label">Your Order:</span>
			<span id="mobile-order-price"></span>
		</div>
		<div class="inside-order">
			<div id="your-price">
				<h3>Your Price</h3>
				<div id="your-price-price">
					<p>Once you've entered your drop-off and pick-up postal codes we'll display the price for your order here.</p>
                </div>
			</div>
			<div id="summary-spacer"></div>
			<div id="your-order" class="group-hidden">
				<h3>Your Order</h3>
				<div id="your-price-products"></div>
				<div id="your-order-extra"></div>
			</div>
			<div style="margin-top: 20px;"><strong>Questions?</strong> Give us a call:<br>1-877-FROGBOX (376-4269)</div>
		</div>
	</div>

</div>
