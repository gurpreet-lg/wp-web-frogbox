		<hr id="footer-line">

		<footer id="footer">
			<div class="row">
				<div class="col-md-4">
          <img src="/wp-content/uploads/2016/03/mini_frog.gif" alt="">
          <div style="display:inline-block;text-align:left;padding-left:10px;">
						<a href="/friends-and-partners/" style="display:block;">Friends and Partners</a>
						<a href="/blog/category/blog/" style="display:block;">Frog Blog</a>
					</div>
        </div>
				<div class="col-md-4">
					<div class="nav-contact" style="text-align: center;">Need help? Call us at
						<a href="tel:+1-877-376-4269" style="cursor: text;font-size: 18px;color: #939597;" class="hoverable">
						<span class="normal">1-877-<span style="color: #8CC640;">FROG</span><span style="color: #00a5c7;">BOX</span></span>
						<span class="hover">1-877-376-4269</span>
						</a>
					</div>
					<p style="font-size:14px;">&copy; Frogbox 2017</p>
				</div>
				<div class="col-md-4">
					<p style="font-size:14px;margin:0;">
					<span><a href="/terms-conditions">Terms & Conditions</a></span>
					<span style="padding:0 5px;"> | </span>
					<span><a href="/privacy-policy">Privacy Policy</a></span>
					</p>
					<ul style="">
						<li><a href="https://plus.google.com/116395028893545391108/about" target="_blank"><img src="http://dev.frogbox.com/wp-content/themes/frogbox/images/ft-social-google.png" alt="" style="width:80%;"></a></li>
						<li><a href="http://www.twitter.com/frogbox" target="_blank"><img src="http://dev.frogbox.com/wp-content/themes/frogbox/images/ft-social-twitter.png" alt="" style="width:80%;"></a></li>
						<li><a href="https://www.facebook.com/frogboxmovingboxes" target="_blank"><img src="http://dev.frogbox.com/wp-content/themes/frogbox/images/ft-social-facebook.png" alt="" style="width:80%;"></a></li>
						<li><a href="http://www.yelp.ca/biz/frogbox-vancouver-2" target="_blank"><img src="http://dev.frogbox.com/wp-content/themes/frogbox/images/ft-social-yelp.png" alt="" style="width:80%;"></a></li>
					</ul>
				</div>
			</div>
		</footer>
	</div> <!-- /container -->


	<div class="modal fade" id="terms-conditions">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title">Terms and Conditions</h4>
	      </div>
	      <div class="modal-body">
	      	<ol>
				<li>Equipment Rented: Frogbox Inc. ("Frogbox") hereby rents to you (the "Renter"), and Renter hereby hires from Frogbox, the equipment specified in the Invoice order details (the "Equipment"). Renter agrees with the count in the above itemized equipment list.</li>
				<li>Rent: Renter shall pay Frogbox rent in the amounts stated in the Renter's Invoice, adjusted for actual rental period.</li>
				<li>Rental Terms: The rental and related charges shall commence upon the date Equipment is delivered by Frogbox Inc. and shall terminate on the date Equipment is picked up by Frogbox Inc.</li>
				<li>Ownership: Equipment is, and shall at all times remain, the sole and exclusive property of Frogbox.</li>
				<li>Warranty: Frogbox warrants that Equipment is in satisfactory operating condition at the time of delivery and will replace (if possible) at no charge, any equipment that fail during normal operation, but not as a result of damage or mishandling. Frogbox is not responsible for the methods or conditions of Equipment operation or for the results obtained.</li>
				<li>Alteration: No alteration to the Equipment may be made without the prior written consent of Frogbox. Proper care and maintenance of the equipment during its use will be the responsibility of RENTER. Equipment, which is returned in a condition requiring cleaning or repairs due to excessive wear and tear or mishandling, will be brought back to rentable condition at the expense of RENTER.</li>
				<li>Assignment: Neither this Agreement nor Equipment may be assigned, transferred, or in any way encumbered by Renter without written consent of Frogbox.</li>
				<li>Risk of Loss: Promptly upon the arrival of Equipment at the Renter's facility, the Renter will carefully inspect the Equipment to determine whether it has been damaged during delivery. In the event of any such damage, the Renter will promptly inform Frogbox and a replacement will be provided. If the Renter shall fail to notify Frogbox of any damages within one business day of the receipt of the Equipment, then the Renter shall be deemed to have accepted the Equipment as being in acceptable operating condition. During the period of the Renter's possession and control of the Equipment, all risk of loss, destruction of, or damage to the Equipment, from any cause whatsoever shall be borne by the Renter.</li>
				<li>Security Deposit: The cost to repair damaged equipment or the replacement for equipment not returned will be charged to the Renter's credit card. Any unpaid rent may also be charged to the Renter's credit card. Customers paying with a cheque will be required to provide a credit card for the security deposit.</li>
				<li>Payment Terms: Due upon delivery by Visa, MC, Amex or Cheque. Rental extensions will be charged automatically and is calculated based on the equipment pick-up date and time.</li>
				<li>Returned Cheque: A $20 NSF fee will apply to a returned cheque. Both the Renter's invoice and NSF fee will be charged to the Renter's credit card.</li>
				<li>Operation: The Renter will use the Equipment in a safe and respectful manner. The Renter shall indemnify and hold Frogbox harmless from any liability whatsoever resulting from the Renters use of the Equipment. Frogbox is not responsible for the results of any loss or damages caused by the Renter's move.</li>
				<li>Default and Remedies: Renter shall be deemed to have breached this Agreement if the Renter: a. Defaults in any payment as set forth in the Renter's order form. b. Defaults in any of the terms herein and such default shall continue uncorrected for five (5) days after written notice hereof to Renter by Frogbox or c. Becomes insolvent, or if a petition is filed by or against Renter under the Bankruptcy and Insolvency Act (Canada) or any other law concerning the relief of debts, or the petition is not discharged within 30 days.</li>
				<li>Termination: In the event of any default, Frogbox may declare the entire amount of unpaid rental payments immediately due and payable, and Frogbox Inc. may immediately terminate this agreement. In the event of such termination, Frogbox Inc. may enter into the premises where Equipment is located and remove same. All costs and expenses to recover equipment and/or rental fees, including legal fees incurred in execution of this section, will be paid by Renter.</li>
			</ol>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

    <?php wp_footer(); ?>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>

		<?php if ( is_page( 'contact' ) ) { ?>
		<script type="text/javascript">
		function GetURLParameter(sParam) {
		    var sPageURL = window.location.search.substring(1);
		    var sURLVariables = sPageURL.split('&');
		    for (var i = 0; i < sURLVariables.length; i++) {
		        var sParameterName = sURLVariables[i].split('=');
		        if (sParameterName[0] == sParam) {
		            return sParameterName[1];
		        }
		    }
		}

		var city = GetURLParameter('city');
		var cityValue = city.replace(/_/g," ");
		jQuery("#vfb-field-10 option:contains(" + cityValue + ")").attr('selected', 'selected');
		</script>
		<?php } ?>

  </body>
</html>
