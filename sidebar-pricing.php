<div id="mobile_menu" class="col-xs-12 col-sm-3 sidebar-mobile clearfix">

	<div id="order-summary" class="sticky sidebar" style="position: relative; top: 0px; width: auto;">
		<div id="mobile-summary" class="group-hidden">
			<button class="c-hamburger c-hamburger--htx reveal-trigger"  href="#mobile_menu"><span>toggle menu</span></button>
			<!-- <span><a class="reveal-trigger btn btn-primary" href="#mobile_menu">Close</a></span> -->
			<span id="mobile-price-label">Your Order:</span>
			<span id="mobile-order-price"></span>
		</div>
		<div class="inside-order">
			<div id="your-price">
				<h3>Your Price</h3>
				<div id="your-price-price">
					<p>Once you've chosen your drop-off and pick-up locations we'll display the price for your order here.</p>
					<p><strong>Moving to a new city?</strong></p>
					<p>With dozens of locations across Canada, the team from any of our locations can pick up the Frogboxes at your new pad.</p>
                	       </div>
			</div>
			<div id="summary-spacer"></div>
			<div id="your-order" class="group-hidden">
				<h3>Your Order</h3>
				<div id="your-price-products"></div>
				<div id="your-order-extra"></div>
			</div>
			<div style="margin-top: 20px;"><strong>Questions?</strong> Give us a call:<br>1-877-FROGBOX (376-4269)</div>
		</div>
	</div>

	<?php if ( ! dynamic_sidebar( 'pricing' ) ): ?>

	<!-- <h3>Sidebar Setup</h3>
	<p>Please add widgets to the blog sidebar to have them display here.</p> -->

	<?php endif; ?>

</div>

<!-- <div class="col-xs-12 col-sm-3 sidebar" style="margin-top:20px;">

	<h4 style="margin-top:40px;text-align: center;">Frogbox</h4>
	<p style="text-align: center;"><img src="/wp-content/uploads/2015/06/the-frogbox-v1.jpg"></p>

	<hr style="margin:40px 0;">

	<h4 style="margin-top:40px;text-align: center;">25 Frogboxes</h4>
	<p style="text-align: center;"><img src="/wp-content/uploads/2015/06/25-frogboxes-v1.jpg"></p>

</div> -->
