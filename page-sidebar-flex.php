<?php
/*
  Template Name: Flex Sidebar Template
*/

?>

<?php get_header(); ?>

  <div class="container-fluid container-inside-text">
	
  <?php if(get_field('custom_page_title')) { ?>
    <div class="row">
      <div class="col-sm-12 page-header"><h1><?php the_field('custom_page_title'); ?></h1></div>
    </div>
  <?php } ?>

    <div class="row">

      <div class="col-xs-12 col-sm-9">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        
        <?php
          /*
          *  Loop through a Flexible Content field and display it's content with different views for different layouts
          */
          while(the_flexible_field("content_builder")): ?>
              
              <?php if(get_row_layout() == "page_content_row"): // layout: Content ?>
                <div class="row">
                  <div class="col-sm-12"><?php the_sub_field("page_content"); ?></div>
                </div>
              <?php elseif(get_row_layout() == "page_image_row"): // layout: Content 
                $image = get_sub_field('page_image');
              ?>
                <div class="row">
                  <div class="col-sm-12"><img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>" /></div>
                </div>
              <?php elseif(get_row_layout() == "page_video_row"): // layout: Content ?>   
                <div class="row">
                  <div class="col-sm-12">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" src="<?php the_sub_field("page_video_url"); ?>"></iframe>
                    </div>
                  </div>
                </div>
              <?php endif; ?>

          <?php endwhile; ?>

          <?php the_content(); ?>

        <?php endwhile; else: ?>

          <div class="page-header">
            <h1>Oh no!</h1>
          </div>

          <p>No content is appearing for this page!</p>

        <?php endif; ?>


      </div>

      <?php get_sidebar(); ?>

    </div>

<?php get_footer(); ?>